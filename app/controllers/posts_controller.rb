class PostsController < ApplicationController
	http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]
	# Action for new posts.
	def new
		@post = Post.new
	end

	# Action for creating new posts.
	def create
		@post = Post.new(post_params)

		if @post.save
			redirect_to @post
		else
			render "new"
		end
	end

	# Action for showing an existing post.
	def show
		@post = Post.find(params[:id])
	end

	# Action for showing a list of all existing blog posts.
	def index
		@posts = Post.all
	end

	# Action for updating an existing post.
	def edit
		@post = Post.find(params[:id])
	end

	# Action for persisting changes to a blog post.
	def update
		@post = Post.find(params[:id])

		if @post.update(params[:post].permit(:title, :text))
			redirect_to @post
		else
			render "edit"
		end
	end

	# Action for removing blog posts.
	def destroy
		@post = Post.find(params[:id])
		@post.destroy

		redirect_to posts_path
	end

	private
		def post_params
			params.require(:post).permit(:title, :text)
		end
end
